import numpy as np
import pandas as pd
import scipy.interpolate as interp
import pyphysio as ph

from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
from matplotlib import cm
from physynch import CrossCorrDistance, WaveletCoherenceCWT

def compute_synch(x, y, w):
    i_st = 0
    i_sp = i_st+w
    
    cc = []
    wc = []
    ec = []
    while i_sp < len(x):
        x_ = x[i_st:i_sp]
        y_ = y[i_st:i_sp]
        
        x_ = (x_ - np.mean(x_)) / np.std(x_)
        y_ = (y_ - np.mean(y_)) / np.std(y_)
        
        cc.append(ccdistance.compute(x_, y_))
        wc.append(wcdistance.compute(x_, y_))
        ec.append(np.linalg.norm(y_ - x_))
        
        i_st+=1
        i_sp+=1
    
    cc = (cc - np.mean(cc)) / np.std(cc)
    wc = (wc - np.mean(wc)) / np.std(wc)
    ec = -(ec - np.mean(ec)) / np.std(ec)
    return(cc, wc, ec)

#%%
synch_data = pd.read_csv('/home/bizzego/Downloads/2023 Best Synchrony Video Analysis - Sheet2.csv').iloc[:121,:]

s1 = ph.create_signal(synch_data['Dir_Man'].values, sampling_freq=2)
s2 = ph.create_signal(synch_data['Dir_Wom'].values, sampling_freq=2)

fsamp=5

s1 = s1.p.resample(fsamp)
s2 = s2.p.resample(fsamp)


ccdistance = CrossCorrDistance(lag=0)
wcdistance = WaveletCoherenceCWT(dt=1/fsamp)

N =len(s1.p.get_values().ravel())

#%%
wsize = int(5*fsamp)
synch = compute_synch(s1.p.get_values().ravel(), 
                      s2.p.get_values().ravel(), 
                      wsize)

synch = np.array(synch).T
# plt.plot(synch)

synch_N = np.nan*(np.zeros((N, 3)))
synch_N[wsize//2: wsize//2+synch.shape[0], :] = synch

synch = synch_N

#%%
# iii = interp.interp1d(np.linspace(wsize//2, N-wsize//2, synch.shape[0]), synch, axis=0)
# synch = iii(np.arange(N))

t = s1.p.get_times()
s1 = s1.p.get_values().ravel()
s2 = s2.p.get_values().ravel()

#%%
   
def init():
    line1.set_data([], [])
    line2.set_data([], [])
    
    line_cc.set_data([], [])
    line_wc.set_data([], [])
    line_ec.set_data([], [])

    return line1, line2, line_cc, line_wc, line_ec

def animate(i):
    x = t[:i]
    y1 = s1[:i]
    y2 = s2[:i]
    cc = synch[:i, 0]
    wc = synch[:i, 1]
    ec = synch[:i, 2]
    
    line1.set_data(x, y1)
    line2.set_data(x, y2)
    line_cc.set_data(x, cc)
    line_wc.set_data(x, wc)
    line_ec.set_data(x, ec)
    
    return line1, line2, line_cc, line_wc, line_ec

Writer = animation.writers['ffmpeg']
writer = Writer(fps=fsamp, metadata=dict(artist='AB'), bitrate=5400)

plt.style.use('fivethirtyeight')
with plt.xkcd(scale = 1, randomness = 5):
    fig = plt.figure(figsize=(16,9))
    ax1 = plt.axes([0.1, 0.55, 0.8, 0.35], xlim=(0, 60), ylim=(0, 5))
    line1, = ax1.plot([], [], lw=3)
    line2, = ax1.plot([], [], lw=3)
    ax1.set_ylabel('Movement Direction')
    ax1.set_yticks([])
    ax1.set_xticks([])
    ax1.legend(['Man', 'Woman'], loc='lower right')
    
    ax2 = plt.axes([0.1, 0.15, 0.8, 0.35], xlim=(0, 60), ylim=(-2.3, 2.3))
    line_cc, = ax2.plot([], [], lw=3, color = 'r')
    line_wc, = ax2.plot([], [], lw=3, color = 'g')
    line_ec, = ax2.plot([], [], lw=3, color = 'b')
    ax2.legend(['Cross-correlation', 'Wavelet Coherence', 'Euclidean'], loc='lower right')
    
    ax2.set_ylabel('Synchrony Level')
    ax2.set_yticks([])
    ax2.set_xticks([])
    ax2.set_xlabel('Time')

    anim = FuncAnimation(fig, animate, init_func=init, frames=N, interval=10, repeat=False, blit=True)

    anim.save('/home/bizzego/synch_5secs.mp4', writer=writer)

