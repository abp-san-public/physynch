import numpy
import numpy as np
import matplotlib.pyplot as plt

import pycwt.wavelet as wavelet

#%%
url = 'http://paos.colorado.edu/research/wavelets/wave_idl/nino3sst.txt'
dat = np.genfromtxt(url, skip_header=19)
title = 'NINO3 Sea Surface Temperature'
label = 'NINO3 SST'
units = 'degC'
t0 = 1871.0
dt = 0.25  # In years

#%%
N = dat.size
t = np.arange(0, N) * dt + t0

#%%
p = np.polyfit(t - t0, dat, 1)
dat_notrend = dat - np.polyval(p, t - t0)
std = dat_notrend.std()  # Standard deviation
var = std ** 2  # Variance
dat_norm = dat_notrend / std  # Normalized dataset

#%%
mother = wavelet.Morlet(6)
wave, scales, freqs, coi, fft, fftfreqs = wavelet.cwt(dat_norm, dt, wavelet=mother)
iwave = wavelet.icwt(wave, scales, dt, wavelet=mother) * std

#%%
power = (numpy.abs(wave)) ** 2
fft_power = numpy.abs(fft) ** 2
period = 1 / freqs
power /= scales[:, None]

#%%
power_coi = power.copy()
for i in range(power.shape[1]):
    period_min = coi[i]
    i_st_na = np.where(period >= period_min)[0][0]
    power_coi[i_st_na:, i] = np.nan


#%%
plt.figure()
plt.subplot(311)
plt.imshow(power)
plt.subplot(312)
plt.contourf(t, freqs, np.log2(power))
plt.plot(t, 1/coi)
plt.yscale('log')

plt.subplot(313)
plt.contourf(t, freqs, np.log2(power_coi))
plt.plot(t, 1/coi)
plt.yscale('log')

#%%
f_low = 0.25
f_high = 1
idx_selected = np.where((freqs>=f_low) & (freqs <=f_high))

plt.figure()
plt.contourf(t, freqs[idx_selected], np.log2(power[idx_selected[0],:]))



#%%
dat_norm2 = dat_norm[::-1]

#%%
wave, aWCT, coi, freqs, sig = wavelet.wct(dat_norm, dat_norm2, dt, sig=False, wavelet = wavelet.Morlet(6))

#%%
power = (numpy.abs(wave)) ** 2
period = 1 / freqs

#%%
power_coi = power.copy()
for i in range(power.shape[1]):
    period_min = coi[i]
    i_st_na = np.where(period >= period_min)[0][0]
    power_coi[i_st_na:, i] = np.nan

#%%
plt.figure()
plt.subplot(311)
plt.imshow(power)
plt.subplot(312)
plt.contourf(t, freqs, np.log2(power))
plt.plot(t, 1/coi)

plt.subplot(313)
plt.contourf(t, freqs, np.log2(power_coi))
plt.plot(t, 1/coi)

#%%
f_low = 0.25
f_high = 1
idx_selected = np.where((freqs>=f_low) & (freqs <=f_high))

plt.figure()
plt.contourf(t, freqs[idx_selected], np.log2(power[idx_selected[0],:]))

#%%
extent = [t.min(), t.max(), 0, max(period)]
plt.plot(t, coi)

bx.set_title('b) {} Wavelet Power Spectrum ({})'.format(label, mother.name))
bx.set_ylabel('Period (years)')
#
Yticks = 2 ** numpy.arange(numpy.ceil(numpy.log2(period.min())),
                           numpy.ceil(numpy.log2(period.max())))
bx.set_yticks(numpy.log2(Yticks))
bx.set_yticklabels(Yticks)

# Third sub-plot, the global wavelet and Fourier power spectra and theoretical
# noise spectra. Note that period scale is logarithmic.
cx = pyplot.axes([0.77, 0.37, 0.2, 0.28], sharey=bx)
cx.plot(glbl_signif, numpy.log2(period), 'k--')
cx.plot(var * fft_theor, numpy.log2(period), '--', color='#cccccc')
cx.plot(var * fft_power, numpy.log2(1./fftfreqs), '-', color='#cccccc',
        linewidth=1.)
cx.plot(var * glbl_power, numpy.log2(period), 'k-', linewidth=1.5)
cx.set_title('c) Global Wavelet Spectrum')
cx.set_xlabel(r'Power [({})^2]'.format(units))
cx.set_xlim([0, glbl_power.max() + var])
cx.set_ylim(numpy.log2([period.min(), period.max()]))
cx.set_yticks(numpy.log2(Yticks))
cx.set_yticklabels(Yticks)
pyplot.setp(cx.get_yticklabels(), visible=False)

# Fourth sub-plot, the scale averaged wavelet spectrum.
dx = pyplot.axes([0.1, 0.07, 0.65, 0.2], sharex=ax)
dx.axhline(scale_avg_signif, color='k', linestyle='--', linewidth=1.)
dx.plot(t, scale_avg, 'k-', linewidth=1.5)
dx.set_title('d) {}--{} year scale-averaged power'.format(2, 8))
dx.set_xlabel('Time (year)')
dx.set_ylabel(r'Average variance [{}]'.format(units))
ax.set_xlim([t.min(), t.max()])

pyplot.show()
