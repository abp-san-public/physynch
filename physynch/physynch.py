import numpy as _np
from scipy.stats import pearsonr, spearmanr
from scipy.signal import correlate, detrend as _detrend   
from scipy import ndimage as _ndimage
import scipy.signal as sp
from statsmodels.tsa.ar_model import AutoReg
import pandas as pd

#%%
def get_lagged(data1, data2, idx_lag):
    #idx_lag is the difference between 
    #the start of data1 and the start of data 2
    if idx_lag == 0:
        data1_out = data1
        data2_out = data2
    
    #idx_lag >0 data1 is delayed
    if idx_lag >0:
        data2_out = data2[idx_lag:]
        data1_out = data1[:-idx_lag]
    
    #idx_lag <0 data2 is anticipated
    if idx_lag <0:
        idx_lag = -idx_lag
        data1_out = data1[idx_lag:]
        data2_out = data2[:-idx_lag]
    return(data1_out, data2_out)
    
def prewhitening(x,y, order=5):
    #NOTE: checked and should be correct
    #still: probably not appropriate for all physio signals
    # see https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6200149
    x = (x - _np.mean(x))/_np.std(x)
    y = (y - _np.mean(y))/_np.std(y)
    
    model = AutoReg(x, lags=order)
    model_fit = model.fit()
    coeffs = -model_fit.params[1:]
    
    pwx = model_fit.resid
    
    pwy = sp.filtfilt([1]+list(coeffs), [1], y)[order:]

    maxlen = min([len(pwx), len(pwy)])
    
    return(pwx[:maxlen], pwy[:maxlen])

def compute_distance(x,y, dist, detrend = True):
    if x is None or y is None: #return none if any signal is none
        return(_np.nan)
    maxlen = _np.min([len(x), len(y)])
    x = x[:maxlen]
    y = y[:maxlen]
    
    if detrend:
        x,y = prewhitening(x, y, 2)

    d = dist.compute(x, y)
    return(d)

def compute_distances_golland(group_1, group_2, surr_mode = 'all', distance='cc', detrend=False, dist_pars = {}):
    assert distance in ['cc', 'mi', 'spearman', 'dtw', 'wct']
    assert surr_mode in ['all', 'equal']
    
    if distance == 'cc':
        dist = CrossCorrDistance(**dist_pars)
    elif distance == 'mi':
        dist = MIDistance(**dist_pars)
    elif distance == 'dtw':
        dist = DTWDistance(**dist_pars)
    elif distance == 'spearman':
        dist = SpearmanDistance(**dist_pars)
    elif distance == 'wct':
        dist = WaveletCoherence(**dist_pars)
        
    ##### COPRESENCE
    copresence = [] #true dyad, true signals
    for I in range(len(group_1)): #iterate over all dyads
        signal_1_I = group_1[I][0]
        signal_2_I = group_2[I][0]
        
        copresence.append(compute_distance(signal_1_I, signal_2_I, dist, detrend)) #true dyad, true signals
    
    copresence = _np.array(copresence)
    copresence_pd = pd.DataFrame(copresence, index = range(len(group_1)), columns=['copresence'])
    copresence_pd['member_1'] = _np.arange(len(group_1))
    copresence_pd['member_2'] = _np.arange(len(group_1))
    
    ##### STIMULUS
    
    #create pairing indexes
    if surr_mode == 'all': #all possible pairs (same dyad excluded)
        indexes_I = []
        indexes_J = []
        for I in range(len(group_1)):
            for J in _np.arange(len(group_1)):
                if I != J: #same dyad excluded ('+1')
                    indexes_I.append(I)
                    indexes_J.append(J)
                
    elif surr_mode == 'equal': #random dyads
        indexes_I = _np.arange(len(group_1))
        done = False
        
        while not done:
            indexes_J = _np.random.permutation(indexes_I)
            if ~(indexes_I == indexes_J).any():
                done=True

   
    stimulus = [] #surrogate dyads, true signals
    for I, J in zip(indexes_I, indexes_J): #iterate over all surrogate dyads
        signal_1_I = group_1[I][0]
        signal_2_J = group_2[J][0]
        
        stimulus.append(compute_distance(signal_1_I, signal_2_J, dist, detrend)) #surrogate dyads, true signals
    
    stimulus = _np.array(stimulus)
    stimulus_pd = pd.DataFrame(stimulus, index = range(len(stimulus)), columns=['stimulus'])
    stimulus_pd['member_1'] = indexes_I
    stimulus_pd['member_2'] = indexes_J
    
    ##### SURROGATE
    #create pairing indexes
    surrogate = []
    if surr_mode == 'all':
        indexes_I = []
        indexes_J = []
        for I in range(len(group_1)):
            for J in _np.arange(I, len(group_1)): #same dyad included
                indexes_I.append(I)
                indexes_J.append(J)
    #else: use the already created pairing indexes
                
    surrogate = [] #surrogate dyad, surrogate signals
    for I, J in zip(indexes_I, indexes_J): #iterate over all surrogate dyads
        signal_1_I_s = group_1[I][1]
        signal_2_J_s = group_2[J][1]
        
        surrogate.append(compute_distance(signal_1_I_s, signal_2_J_s, dist, detrend)) 
            
    surrogate = _np.array(surrogate)
    surrogate_pd = pd.DataFrame(surrogate, index = range(len(surrogate)), columns=['surrogate'])
    surrogate_pd['member_1'] = indexes_I
    surrogate_pd['member_2'] = indexes_J

    
    return(copresence_pd, stimulus_pd, surrogate_pd)

class DTWDistance(object):
    def __init__(self, method='Euclidean',step='asymmetric', wtype='sakoechiba', openend=True, openbegin=True, wsize=5, normalize = True, report_similarity = True):
        import rpy2.robjects.numpy2ri
        from rpy2.robjects.packages import importr
        rpy2.robjects.numpy2ri.activate()
        
        # Set up our R namespaces
        self.R = rpy2.robjects.r
        self.DTW = importr('dtw')
        self.method = method
        self.step = step
        self.dtwstep = getattr(self.DTW, self.step)
        self.wtype = wtype
        self.openend = openend
        self.openbegin = openbegin
        self.wsize = wsize
        self.normalize = normalize
        self.report_similarity = report_similarity
        
    def compute(self, x, y):
        # Calculate the alignment vector and corresponding distance
        
        
        # alignment = R.dtw(x, y, dist_method='Euclidean')
        # dist = alignment.rx('distance')[0][0]
        # #%%
        # alignment = R.dtw(x, y, 
        #                   # dist_method='Euclidean', 
        #                   # step_pattern=dtwstep, 
        #                   # window_type='sakoechiba', 
        #                   # keep_internals=False, 
        #                   # distance_only=True, 
        #                   # open_end=True, 
        #                   # open_begin=True, 
        #                   **{'dist.method' : 'Euclidean',
        #                      'window.type' : 'sakoechiba',
        #                      'open.end' : True,
        #                      # 'open.begin' : True,
        #                      'window.size': 8,
        #                      # 'step.pattern': 'asymmetric'
        #                      })
        # dist = alignment.rx('distance')[0][0]
        
        # print(len(x), len(y))
        alignment = self.R.dtw(x, y, dist_method=self.method, step_pattern=self.dtwstep, window_type=self.wtype, keep_internals=False, distance_only=True, open_end=self.openend, open_begin=self.openbegin, **{'window.size': self.wsize})
        dist = alignment.rx('distance')[0][0]
        
        if self.normalize:
            dist = dist/len(x)
        
        if self.report_similarity:
            dist = -dist
        return(dist)

class CrossCorrDistance(object):
    def __init__(self, lag = 10, normalize = True):
        self.lag = lag
        self.normalize = normalize
    
    def compute(self, data1, data2):
        
        dist_lags = []
        for curr_lag in _np.arange(-self.lag, self.lag+1):
            data1_lag, data2_lag = get_lagged(data1, data2, curr_lag)
            
            if self.normalize:
                data1_lag = (data1_lag - _np.mean(data1_lag))/_np.std(data1_lag)
                data2_lag = (data2_lag - _np.mean(data2_lag))/_np.std(data2_lag)
                
            c = correlate(data1_lag, data2_lag, mode='valid')
            c = c/len(data1_lag)
            if self.normalize: #redundant as if self.normalize, stds = 0
                c = c/(_np.std(data1_lag)*_np.std(data2_lag))
            
            # print(c)
            dist_lags.append(c)
        
        #TODO ABS??
        dist_lags = _np.array(dist_lags)
        # dist = dist_lags[_np.argmax(abs(dist_lags))]
        dist = dist_lags[_np.argmax(dist_lags)]
        
        return(dist[0])

class MIDistance(object):
    def __init__(self, K=4, lag=0, jarlocation = ''):
        import jpype
        self.jarLocation = jarlocation
        
        if not jpype.isJVMStarted():
            jpype.startJVM(jpype.getDefaultJVMPath(), "-ea", "-Djava.class.path=" + self.jarLocation)
        MIcalcClass = jpype.JPackage("infodynamics.measures.continuous.kraskov").MutualInfoCalculatorMultiVariateKraskov2
        self.MIcalc = MIcalcClass()
        # 2. Set any properties to non-default values:
        self.MIcalc.setProperty("NORMALISE", "true")
        self.MIcalc.setProperty("k", str(K))
        self.K = K
        self.lag = lag
        
        
    def compute(self, data1, data2):
        dist_lags = []
        for curr_lag in _np.arange(-self.lag, self.lag+1):
            data1_lag, data2_lag = get_lagged(data1, data2, curr_lag)
            self.MIcalc.initialise()
            self.MIcalc.setObservations([[x] for x in data1_lag], [[x] for x in data2_lag])
            MI = self.MIcalc.computeAverageLocalOfObservations()
            dist_lags.append(MI)
        return(_np.max(dist_lags))

class PearsonDistance(object):
    def compute(self, data1, data2):
        # Calculate the alignment vector and corresponding distance
        x = list(data1)
        y = list(data2)
    
        r, p = pearsonr(x, y)
        return(r, p)

class SpearmanDistance(object):
    def compute(self, data1, data2):
        # Calculate the alignment vector and corresponding distance
        x = list(data1)
        y = list(data2)
    
        r, p = spearmanr(x, y)
        return(r, p)

#%%
class WaveletCoherence(object):
    def __init__(self, fsamp, wtype = 'cgau1', lag = 0, f_low = 0, f_high = None, normalize=True):
        '''
        dt: 1/fsamp of the input signals (signals must have the same fsamp)
        '''
        # import pycwt.wavelet as wavelet
        # self.wavelet = wavelet
        self.fsamp = fsamp
        self.wtype = wtype
        self.lag = lag
        self.f_low = f_low
        self.f_high = 0.5*(fsamp) if f_high is None else f_high
        self.normalize = normalize
    
    def xwt_coherence(self, x1, x2, nNotes=12, detrend=True, normalize=True, sigma=2):
        #code from https://gist.github.com/aprovecharLab/7654032c64370672b933559f887afd56
        
        #%%
        import pywt
        
        N1 = len(x1)
        N2 = len(x2)
        assert (N1 == N2),   "error: arrays not same size"
        
        N = N1
        dt = 1.0 / self.fsamp
        # dt = 1.0 / fsamp
        times = _np.arange(N) * dt

        # detrend and normalize
        if detrend:
            x1 = _detrend(x1,type='linear')
            x2 = _detrend(x2,type='linear')
        if normalize:
            stddev1 = x1.std()
            x1 = x1 / stddev1
            stddev2 = x2.std()
            x2 = x2 / stddev2

        # Define some parameters of our wavelet analysis. 
        nOctaves = int(_np.log2(2*N//2))
        scales = 2**_np.arange(1, nOctaves, 1.0/nNotes)

        # cwt
        coef1, freqs =pywt.cwt(x1, scales, self.wtype)
        coef2, freqs =pywt.cwt(x2, scales, self.wtype)

        # Calculates the cross transform of xs1 and xs2.
        coef12 = coef1 * _np.conj(coef2)
        
        # coherence
        scaleMatrix = _np.ones([1, N]) * scales[:, None]

        S1 = _ndimage.gaussian_filter((_np.abs(coef1)**2 / scaleMatrix), sigma=sigma)
        S2 = _ndimage.gaussian_filter((_np.abs(coef2)**2 / scaleMatrix), sigma=sigma)
        S12 = _ndimage.gaussian_filter((_np.abs(coef12 / scaleMatrix)), sigma=sigma)
        
        WCT = S12**2 / (S1 * S2)
        
        #%%
        # import matplotlib.pyplot as plt
        # plt.figure()
        # plt.imshow(abs(WCT), aspect='auto')
        # plt.colorbar()

        #%%
        # cone of influence in frequencies (conservative version)
        frequencies = freqs / dt
        
        coif_ = 1/(2*times[1:len(times)//2])
        min_coif = coif_[-1]
        
        coif = _np.zeros(len(times)) + min_coif
        
        coif[:len(coif_)] = coif_
        coif[-len(coif_):] = coif_[::-1]
        
        # plt.plot(frequencies, _np.mean(WCT, axis=1), '.-')

        #%%
        return WCT, times, frequencies, coif
    
    def compute(self, data1, data2):
        
        dist_lags = []
        for curr_lag in _np.arange(-self.lag, self.lag+1):
            data1_lag, data2_lag = get_lagged(data1, data2, curr_lag)
            
            # WCT, aWCT, coi, freq, sig = self.wavelet.wct(data1, data2, self.dt, sig=False, wavelet=self.wavelet.Morlet(6))
            
            WCT, times, frequencies, coif = self.xwt_coherence(data1_lag, data2_lag, nNotes=12,
                                                               detrend=True, normalize=True)

            #select coi
            WCT_coi = WCT.copy()
            for i in range(WCT.shape[1]):
                idx_na = _np.where(frequencies < coif[i])[0]
                WCT_coi[idx_na, i] = _np.nan

            #select freqs
            idx_freqs = _np.where((frequencies >= self.f_low) & \
                                  (frequencies <= self.f_high))[0]
                
            WCT_freqs = WCT_coi[idx_freqs, :]

            dist = _np.nansum(WCT_freqs)
            
            if self.normalize:
                n_valid = (_np.sum(~_np.isnan(WCT_freqs)))
                dist = dist/n_valid
                
            dist_lags.append(dist)
        
        return(_np.max(dist_lags))
    
class WaveletCoherenceCWT(object):
    def __init__(self, dt, lag=0, f_low = 0, f_high = None, normalize=True):
        '''
        dt: 1/fsamp of the input signals (signals must have the same fsamp)
        '''
        
        self.dt = dt
        self.lag = lag
        self.f_low = f_low
        self.f_high = 0.5*(1/dt) if f_high is None else f_high
        self.normalize = normalize
        
    def compute(self, data1, data2):
        import pycwt.wavelet as wavelet
        
        dist_lags = []
        for curr_lag in _np.arange(-self.lag, self.lag+1):
            data1_lag, data2_lag = get_lagged(data1, data2, curr_lag)
                        
            WCT, aWCT, coi, freq, sig = wavelet.wct(data1_lag, data2_lag, 
                                                    self.dt, sig=False, 
                                                    wavelet=wavelet.Morlet(6))
            # WCT, aWCT, coi, freq, sig = wavelet.wct(data1_lag, data2_lag, dt, sig=False, wavelet=wavelet.Morlet(6))
            
            period = 1 / freq
            
            #select coi
            WCT_coi = WCT.copy()
            for i in range(WCT.shape[1]):
                period_min = coi[i]
                i_st_na = _np.where(period >= period_min)[0][0]
                WCT_coi[i_st_na:, i] = _np.nan
            
            #select freqs
            idx_selected = _np.where((freq>=self.f_low) & (freq <=self.f_high))[0]
            # idx_selected = _np.where((freq>=f_low) & (freq <=f_high))[0]
            WCT_freqs = WCT_coi[idx_selected, :]
            
            dist = _np.nansum(WCT_freqs)
            
            if self.normalize:
                n_valid = (_np.sum(~_np.isnan(WCT_freqs)))
                dist = dist/n_valid
                
            dist_lags.append(dist)
        
        return(_np.max(dist_lags))
