import numpy as np
import matplotlib.pyplot as plt
import pycwt as wav

import pyphysio as ph

#%%
s1 = ph.from_pickle('/home/bizzego/UniTn/data/nirs_parenting_synch/with_MF/mothers/deoxy_clusters/D37_C/3/MedialRight.pkl')
t0 = 0
dt = 1/s1.get_sampling_freq()


#%%
url = 'http://paos.colorado.edu/research/wavelets/wave_idl/nino3sst.txt'
s1 = np.genfromtxt(url, skip_header=19)
title = 'NINO3 Sea Surface Temperature'
label = 'NINO3 SST'
units = 'degC'
t0 = 1871.0
dt = 0.25  # In years

#%%
def compute_wavelet(dat, dt, t0):
    N = dat.size
    t = np.arange(0, N) * dt + t0
    
    #% detrend
    p = np.polyfit(t - t0, dat, 1)
    dat_notrend = dat - np.polyval(p, t - t0)
    std = dat_notrend.std()  # Standard deviation
    dat_norm = dat_notrend / std  # Normalized dataset
    
    #% wavelet
    mother = wav.Morlet(6)
    wave, scales, freqs, coi, fft, fftfreqs = wav.cwt(dat_norm, dt, wavelet=mother)
    
    return(wave, scales, freqs, coi, fft, fftfreqs)
    
#%%    
wave, scales, freqs, coi, fft, fftfreqs = compute_wavelet(s1, dt, t0)
power = (np.abs(wave)) ** 2
period = 1 / freqs
power /= scales[:, None]

N = s1.size
t = np.arange(0, N) * dt + t0

#%%
plt.figure()
plt.contourf(t, freqs, power)
plt.colorbar()
plt.yscale('log')

#%%
plt.figure()
plt.plot(power[0,:])
plt.plot(power[-1,:])

#%%
    
power = (np.abs(wave)) ** 2
period = 1 / freqs
power /= scales[:, None]

power_ = np.copy(power)
for i in range(power.shape[1]):
    period_min = coi[i]
    i_st_na = np.where(period >= period_min)[0][0]
    power_[i_st_na:, i] = np.nan

plt.figure()
plt.subplot(211)
plt.contour(np.log2(power))
#    plt.plot(t, np.log2(coi))
plt.subplot(212)
plt.contourf(t, freqs, np.log2(power), cmap=plt.cm.viridis)
#    bx.contourf(t, freqs, np.log2(power_),cmap=plt.cm.viridis)
#    plt.plot(t, np.log2(coi))

#    print(power)
    
