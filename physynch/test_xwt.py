import pyphysio as ph
import os
import numpy as np
import numpy as _np
import pandas as pd
from physynch import surrogate_IAAFT, WaveletCoherence
from pyphysio.specialized.fnirs import load_xrnirs
from scipy.signal import correlate, detrend as _detrend  
from config import conditions, fsamp, cohorts, members
from local_config import root_datadir
i_cp = 0 #Oxy

import matplotlib.pyplot as plt
from scipy import ndimage as _ndimage

import pywt

#%%
COHORT = 'mothers'
DYAD = 'NSFD1'
CONDITION = 'play'
nNotes = 12
distance = WaveletCoherence(fsamp, lag = 0, f_low=0.5, f_high=3, normalize=True)

synchrony = []
i_cl=0
DATA_FOLDER = f'{root_datadir}/processed/imported/{COHORT}'

#%%
nirs_A = load_xrnirs(f'{DATA_FOLDER}/{DYAD}/{CONDITION}/parent_clusters')
nirs_B = load_xrnirs(f'{DATA_FOLDER}/{DYAD}/{CONDITION}/child_clusters')
 
x1 = nirs_A.p.main_signal.values[:,i_cl,i_cp].ravel()
x2 = nirs_B.p.main_signal.values[:,i_cl,i_cp].ravel()

# sig2=sig1
assert np.sum(np.isnan(x1)) == 0
assert np.sum(np.isnan(x2)) == 0

N1 = len(x1)
N2 = len(x2)
assert (N1 == N2),   "error: arrays not same size"

#%%
N = N1
dt = 1.0 / fsamp
# dt = 1.0 / fsamp
times = _np.arange(N) * dt

detrend=True
normalize=True
###########################################################################
# detrend and normalize
if detrend:
    x1 = _detrend(x1,type='linear')
    x2 = _detrend(x2,type='linear')
if normalize:
    stddev1 = x1.std()
    x1 = x1 / stddev1
    stddev2 = x2.std()
    x2 = x2 / stddev2

###########################################################################
# Define some parameters of our wavelet analysis. 

# maximum range of scales that makes sense
# min = 2 ... Nyquist frequency
# max = np.floor(N/2)

#%%
nOctaves = int(_np.log2(2*_np.floor(N/2.0)))
scales = 2**_np.arange(1, nOctaves, 1.0/nNotes)

###########################################################################
# cwt and the frequencies used. 
# Use the complex morelet with bw=1.5 and center frequency of 1.0
coef1, freqs1=pywt.cwt(x1,scales,'cmor_1.5-1.0')
coef2, freqs2=pywt.cwt(x2,scales,'cmor_1.5-1.0')

###########################################################################
# Calculates the cross transform of xs1 and xs2.
coef12 = coef1 * _np.conj(coef2)

#%%
###########################################################################
# coherence
scaleMatrix = _np.ones([1, N]) * scales[:, None]

S1 = _ndimage.gaussian_filter((_np.abs(coef1)**2 / scaleMatrix), sigma=1)
S2 = _ndimage.gaussian_filter((_np.abs(coef2)**2 / scaleMatrix), sigma=1)
S12 = _ndimage.gaussian_filter((_np.abs(coef12 / scaleMatrix)), sigma=1)

WCT = S12**2 / (S1 * S2)

#%%
plt.figure()
plt.imshow(abs(WCT), aspect='auto')
plt.colorbar()

#%%
# cone of influence in periods (?) for mexhat wavelet (???)
coif_ = 1/(2*times[1:len(times)//2])
max_coif = coif_[0]

coif = _np.zeros(len(times)) + max_coif

coif[1:1+len(coif_)] = coif_
coif[1+len(coif_)] = coif_[-1]
coif[-(len(coif_)+1):-1] = coif_[::-1]

#%%
# frequencies used
frequencies = pywt.scale2frequency('cmor_1.5-1.0', scales) / dt

plt.plot(frequencies, np.mean(WCT, axis=1), '.-')


#%%
fig, ax = plt.subplots()
xx,yy = np.meshgrid(times, frequencies)
ZZ = WCT

im = ax.pcolor(xx,yy,ZZ)
ax.plot(times,coif)
ax.fill_between(times,coif, step="mid", alpha=0.4)

ax.set_xlim(times.min(), times.max())
ax.set_ylim(frequencies.min(), frequencies.max())
