import numpy as _np
import statsmodels.tsa.api as smt

#%%%
# IAAFT surrogates
#from : https://github.com/manu-mannattil/nolitsa/blob/master/nolitsa/surrogates.py

def _ft(x):
    """Return simple Fourier transform surrogates.

    Returns phase randomized (FT) surrogates that preserve the power
    spectrum (or equivalently the linear correlations), but completely
    destroy the probability distribution.

    Parameters
    ----------
    x : array
        Real input array containg the time series.

    Returns
    -------
    y : array
        Surrogates with the same power spectrum as x.
    """
    y = _np.fft.rfft(x, axis = 0)

    phi = 2 * _np.pi * _np.random.random(y.shape[0])

    phi[0] = 0.0
    if x.shape[0] % 2 == 0:
        phi[-1] = 0.0

    y = y * _np.exp(1j * phi)
    
    return _np.fft.irfft(y, n=x.shape[0], axis=0)

def surrogate_AAFT(x):
    """Return amplitude adjusted Fourier transform surrogates.

    Returns phase randomized, amplitude adjusted (AAFT) surrogates with
    crudely the same power spectrum and distribution as the original
    data (Theiler et al. 1992).  AAFT surrogates are used in testing
    the null hypothesis that the input series is correlated Gaussian
    noise transformed by a monotonic time-independent measuring
    function.

    Parameters
    ----------
    x : array
        1-D input array containg the time series.

    Returns
    -------
    y : array
        Surrogate series with (crudely) the same power spectrum and
        distribution.
    """
    # Generate uncorrelated Gaussian random numbers.
    y = _np.random.normal(size=x.shape[0])

    # Introduce correlations in the random numbers by rank ordering.
    y = _np.sort(y)[_np.argsort(_np.argsort(x, axis=0), axis=0)]
    
    y = _ft(y)

    return _np.sort(x, axis=0)[_np.argsort(_np.argsort(y, axis = 0), axis=0)]

def surrogate_IAAFT(x, maxiter=1000, atol=1e-8, rtol=1e-10):
    """Return iterative amplitude adjusted Fourier transform surrogates.

    Returns phase randomized, amplitude adjusted (IAAFT) surrogates with
    the same power spectrum (to a very high accuracy) and distribution
    as the original data using an iterative scheme (Schreiber & Schmitz
    1996).

    Parameters
    ----------
    x : array
        1-D real input array of length N containing the time series.
    maxiter : int, optional (default = 1000)
        Maximum iterations to be performed while checking for
        convergence.  The scheme may converge before this number as
        well (see Notes).
    atol : float, optional (default = 1e-8)
        Absolute tolerance for checking convergence (see Notes).
    rtol : float, optional (default = 1e-10)
        Relative tolerance for checking convergence (see Notes).

    Returns
    -------
    y : array
        Surrogate series with (almost) the same power spectrum and
        distribution.
    i : int
        Number of iterations that have been performed.
    e : float
        Root-mean-square deviation (RMSD) between the absolute squares
        of the Fourier amplitudes of the surrogate series and that of
        the original series.

    Notes
    -----
    To check if the power spectrum has converged, we see if the absolute
    difference between the current (cerr) and previous (perr) RMSDs is
    within the limits set by the tolerance levels, i.e., if abs(cerr -
    perr) <= atol + rtol*perr.  This follows the convention used in
    the NumPy function numpy.allclose().

    Additionally, atol and rtol can be both set to zero in which
    case the iterations end only when the RMSD stops changing or when
    maxiter is reached.
    """
    # Calculate "true" Fourier amplitudes and sort the series.
    ampl = _np.abs(_np.fft.rfft(x, axis = 0))
    sort = _np.sort(x, axis = 0)

    # Previous and current error.
    perr, cerr = (-1, 1)

    # Start with a random permutation.
    t = _np.fft.rfft(_np.random.permutation(x))

    for i in range(maxiter):
        # Match power spectrum.
        s = _np.real(_np.fft.irfft(ampl * t / _np.abs(t), n=x.shape[0]))

        # Match distribution by rank ordering.
        y = sort[_np.argsort(_np.argsort(s, axis=0))]

        t = _np.fft.rfft(y)
        cerr = _np.sqrt(_np.mean((ampl ** 2 - _np.abs(t) ** 2) ** 2))

        # Check convergence.
        if abs(cerr - perr) <= atol + rtol * abs(perr):
            break
        else:
            perr = cerr

    # Normalize error w.r.t. mean of the "true" power spectrum.
    return y, i, cerr / _np.mean(ampl ** 2)

#%%%
# AR process
def _fit_AR(x, maxlag=10):
    mdl = smt.AR(x).fit(maxlag=maxlag, ic='aic', trend='nc')
    return(mdl.params)

def _sim_AR(alpha, n, ndisc):
    ar = _np.r_[1, -alpha]
    ma = _np.r_[1,0]
    x = smt.arma_generate_sample(ar=ar, ma=ma, nsample=n, burnin = ndisc) 
    return(x)

def surrogate_ARMA(x, maxlag = 30, ndisc = 1000, estimate=True):
    if estimate:
        est_lag = smt.AR(x).select_order(maxlag=maxlag, ic='aic', trend='c')
        print(est_lag)
    else:
        est_lag = maxlag
    
    mean_x = _np.mean(x)
    std_x = _np.std(x)
    x = (x- mean_x)/std_x
    
    alpha = _fit_AR(x, est_lag)
    n = len(x)
    x_surr = _sim_AR(alpha, n, ndisc=ndisc)
    x_surr = (x_surr - _np.mean(x_surr))/_np.std(x_surr)
    x_surr = x_surr*std_x + mean_x
    return(x_surr)
