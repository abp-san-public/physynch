import numpy
import matplotlib.pyplot as plt
import pycwt.wavelet as wav
from pycwt.helpers import find
import numpy as np

#%%
def detrend(x, t, t0, normalize=True):
#    t = x.get_times()
#    t0 = x.get_start_time()
    p = np.polyfit(t - t0, x, 1)
    x_notrend = x - np.polyval(p, t - t0)
    if normalize:
        std = x_notrend.std()  # Standard deviation
        x_norm = x_notrend / std  # Normalized dataset
    return(x_norm)

def wavelet_final(x, dt, t, t0):
#    dt = 1/x.get_sampling_freq()
    
    x_detr = detrend(x, t, t0)
    
    mother = wav.Morlet(6)
    
    wave, scales, freqs, coi, fft, fftfreqs = wav.cwt(x_detr, dt=dt, wavelet=mother)
    
    power = (np.abs(wave)) ** 2
    period = 1 / freqs
    power /= scales[:, None] 
    
    power_coi = power.copy()
    for i in range(power.shape[1]):
        period_min = coi[i]
        i_st_na = np.where(period >= period_min)[0][0]
        power_coi[i_st_na:, i] = np.nan
    
    return(power_coi)
    
#%%
url = 'http://paos.colorado.edu/research/wavelets/wave_idl/nino3sst.txt'
dat = numpy.genfromtxt(url, skip_header=19)
title = 'NINO3 Sea Surface Temperature'
label = 'NINO3 SST'
units = 'degC'
t0 = 1871.0
dt = 0.25  # In years

#%%
N = dat.size
t = numpy.arange(0, N) * dt + t0

w1 = wavelet_final(dat, dt, t, t0)

#%%
p = numpy.polyfit(t - t0, dat, 1)
dat_notrend = dat - numpy.polyval(p, t - t0)
std = dat_notrend.std()  # Standard deviation
var = std ** 2  # Variance
dat_norm = dat_notrend / std  # Normalized dataset

#%%
mother = wav.Morlet(6)
s0 = 2 * dt  # Starting scale, in this case 2 * 0.25 years = 6 months
dj = 1 / 12  # Twelve sub-octaves per octaves
J = 7 / dj  # Seven powers of two with dj sub-octaves

#%%
wave, scales, freqs, coi, fft, fftfreqs = wav.cwt(dat_norm, dt, dj, s0, J, mother)
iwave = wav.icwt(wave, scales, dt, dj, mother) * std

#%%
power = (numpy.abs(wave)) ** 2
fft_power = numpy.abs(fft) ** 2
period = 1 / freqs
power /= scales[:, None]

#%%
# Prepare the figure
# Second sub-plot, the normalized wavelet power spectrum and significance
# level contour lines and cone of influece hatched area. Note that period
# scale is logarithmic.
plt.figure()
bx = plt.subplot(111)

#levels = [0.0625, 0.125, 0.25, 0.5, 1, 2, 4, 8, 16]
bx.contourf(numpy.log2(power_))
plt.plot(numpy.arange(len(coi)),coi, 'k')

#%%
power_ = power.copy()
for i in range(power.shape[1]):
    period_min = coi[i]
    i_st_na = np.where(period >= period_min)[0][0]
    power_[i_st_na:, i] = 0

#%%    
# Third sub-plot, the global wavelet and Fourier power spectra and theoretical
# noise spectra. Note that period scale is logarithmic.
cx = pyplot.axes([0.77, 0.37, 0.2, 0.28], sharey=bx)
cx.plot(glbl_signif, numpy.log2(period), 'k--')
cx.plot(var * fft_theor, numpy.log2(period), '--', color='#cccccc')
cx.plot(var * fft_power, numpy.log2(1./fftfreqs), '-', color='#cccccc',
        linewidth=1.)
cx.plot(var * glbl_power, numpy.log2(period), 'k-', linewidth=1.5)
cx.set_title('c) Global Wavelet Spectrum')
cx.set_xlabel(r'Power [({})^2]'.format(units))
cx.set_xlim([0, glbl_power.max() + var])
cx.set_ylim(numpy.log2([period.min(), period.max()]))
cx.set_yticks(numpy.log2(Yticks))
cx.set_yticklabels(Yticks)
pyplot.setp(cx.get_yticklabels(), visible=False)

# Fourth sub-plot, the scale averaged wavelet spectrum.
dx = pyplot.axes([0.1, 0.07, 0.65, 0.2], sharex=ax)
dx.axhline(scale_avg_signif, color='k', linestyle='--', linewidth=1.)
dx.plot(t, scale_avg, 'k-', linewidth=1.5)
dx.set_title('d) {}--{} year scale-averaged power'.format(2, 8))
dx.set_xlabel('Time (year)')
dx.set_ylabel(r'Average variance [{}]'.format(units))
ax.set_xlim([t.min(), t.max()])

pyplot.show()
