import numpy as np
import matplotlib.pyplot as plt

#%%
synch_data = np.loadtxt('/home/bizzego/synch.txt')

with plt.xkcd(scale = 0.5, randomness = 10):
    ax1 = plt.subplot(211)
    plt.plot(synch_data[:,0], synch_data[:,1])
    plt.ylabel('fNIRS - MOTHER')

    frame1 = plt.gca()
    frame1.axes.yaxis.set_ticklabels([])
    frame1.axes.yaxis.set_ticks([])
    
    ax2 = plt.subplot(212, sharex=ax1)
    plt.plot(synch_data[:,0], synch_data[:,2])
    plt.ylabel('fNIRS - CHILD')

    frame1 = plt.gca()
    frame1.axes.yaxis.set_ticklabels([])
    frame1.axes.yaxis.set_ticks([])
    
    plt.xlabel('TIME - seconds')

plt.tight_layout()
plt.show()



#%%
asynch_data = np.loadtxt('/home/bizzego/asynch.txt')

with plt.xkcd(scale = 0.5, randomness = 10):
    ax1 = plt.subplot(211)
    plt.plot(asynch_data[:,0], asynch_data[:,1])
    plt.ylabel('fNIRS - MOTHER')

    frame1 = plt.gca()
    frame1.axes.yaxis.set_ticklabels([])
    frame1.axes.yaxis.set_ticks([])
    
    ax2 = plt.subplot(212, sharex=ax1)
    plt.plot(asynch_data[:,0], asynch_data[:,2])
    plt.ylabel('fNIRS - CHILD')

    frame1 = plt.gca()
    frame1.axes.yaxis.set_ticklabels([])
    frame1.axes.yaxis.set_ticks([])
    
    plt.xlabel('TIME - seconds')



plt.tight_layout()
plt.show()