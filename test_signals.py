import numpy as np
import matplotlib.pyplot as plt
import pyphysio as ph

def random_signal(A, W, P, fsamp = 8, tmax = 10, idx_random=100):
    t=np.arange(0, tmax, 1/fsamp)
    
    s = []
    for a, w, p in zip(A, W, P):
        s_ = a*np.sin(w*2*np.pi * t + p/180*np.pi)
        s.append(s_)
    
    s = np.array(s)
    s = np.sum(s, axis=0)
    
#    idx_random = int(np.random.uniform(0, 546 - len(t)))
    s = s + true_sig_f[idx_random:idx_random+len(t)]
    
    return(t, s)

#%%
true_sig = ph.from_pickle('/home/bizzego/UniTn/data/nirs_MC/video/raw_sqi/NSFD9V_s2_3.pkl')
true_sig = true_sig.get_channel(35)


true_sig_f = ph.Normalize()(ph.IIRFilter([.2, .6], [.1, 2])(true_sig))
true_sig_f = true_sig_f.get_values()

#%%    
tmax=10

plt.close()
A1 = [5.0, 1.5]
W1 = [0.13, 0.35]
P1 = [15, 50]
t1, s1_s = random_signal(A1, W1, P1, idx_random=100)

A2 = [4.5, 1.25]
W2 = [0.15, 0.27]
P2 = [0, 20]
t2, s2_s = random_signal(A2, W2, P2, idx_random= 0)

A1 = [2.0, 2.0]
W1 = [0.1, 0.15]
P1 = [15, 0]
t1, s1_as = random_signal(A1, W1, P1, idx_random=25)

A2 = [2, 2]
W2 = [0.05, 0.2]
P2 = [-90, 20]
t2, s2_as = random_signal(A2, W2, P2, idx_random=14)

t = np.hstack([t1,t2+tmax])
s1 = np.hstack([s1_s,-1*s1_as])
s2 = np.hstack([s2_s,s2_as])

plt.plot(t, s1)
plt.plot(t, s2)

#%%
np.savetxt('/home/bizzego/synch_data.txt', np.c_[t, s1, s2])
