# coding=utf-8
from setuptools import setup

setup(
    name='physynch',
    packages=['physynch'],
    version='0.1',
    description='Python library for computation of synchrony with Golland\'s framework',
    author='Andrea Bizzego',
    author_email='a.bizzego@gmail.com',
    install_requires=[
        'numpy',
        'scipy']
)
